package br.com.lgovea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lgovea.model.Cep;
import br.com.lgovea.service.EnderecoService;

@RestController
@RequestMapping("/api/v1/cep")
public class CepController {

	@Autowired
	private EnderecoService enderecoService;

	@GetMapping(value = "/{cep}")
	public ResponseEntity<Cep> getCep(@PathVariable Integer cep) {
		return new ResponseEntity<Cep>(enderecoService.buscarCep(cep.toString()), HttpStatus.OK);
	}
	
}
