package br.com.lgovea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lgovea.feignclient.CepClient;
import br.com.lgovea.model.Cep;

@Service
public class EnderecoService {

	@Autowired
	private CepClient cepCliente;
	
	public Cep buscarCep(String cep) {
		return cepCliente.getCep(cep);
	}
	
}
