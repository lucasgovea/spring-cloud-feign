# Descrição

Projeto de backend API REST de exemplo criado utilizando as seguintes tecnologia:  
- Spring boot  
- Spring OpenFeign  

Consulta a API de CEP Postmon (https://postmon.com.br/)



A API não faz validação e tratamentos de erros, é uma implementacao básica para ver o funcionamento.


Na URI /api/v1/cep/<cep_desejado> podemos obter os dados sobre o CEP 
##### Buscar CEP

``` curl --request GET \
  --url http://localhost:8080/api/v1/cep/<cep_desejado> ```

